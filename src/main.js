var divs = document.getElementsByClassName("project")
var winWidth = window.innerWidth
var winHeight = window.innerHeight

window.onresize = updatePos

for (var i = 0; i < divs.length; i++) {
  var thisDiv = divs[i]
  randomTop = getRandomNumber(64, winHeight - 128)
  randomLeft = getRandomNumber(64, winWidth - 128)
  thisDiv.style.top = randomTop + "px"
  thisDiv.style.left = randomLeft + "px"
}

function updatePos() {
  for (var i = 0; i < divs.length; i++) {
    var thisDiv = divs[i]
    randomTop = getRandomNumber(128, winHeight - 256)
    randomLeft = getRandomNumber(128, winWidth - 256)
    thisDiv.style.top = randomTop + "px"
    thisDiv.style.left = randomLeft + "px"
  }
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min
}

var draggableElements = document.getElementsByClassName("draggable")

for (var i = 0; i < draggableElements.length; i++) {
  dragElement(draggableElements[i])
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0
  if (document.getElementById(elmnt.id + "header")) {
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown
  } else {
    elmnt.onmousedown = dragMouseDown
  }

  function dragMouseDown(e) {
    e = e || window.event
    pos3 = parseInt(e.clientX)
    pos4 = parseInt(e.clientY)
    document.onmouseup = closeDragElement
    document.onmousemove = elementDrag
    return false
  }

  function elementDrag(e) {
    e = e || window.event
    pos1 = pos3 - parseInt(e.clientX)
    pos2 = pos4 - parseInt(e.clientY)
    pos3 = parseInt(e.clientX)
    pos4 = parseInt(e.clientY)
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px"
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px"
  }

  function closeDragElement() {
    document.onmouseup = null
    document.onmousemove = null
  }
}
